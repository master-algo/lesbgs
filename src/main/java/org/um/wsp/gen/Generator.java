package org.um.wsp.gen;

import org.um.wsp.model.*;

import java.time.Duration;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Generator {
    private Generator(){
    }

    public static WSPModel modelRandom(int nbJob, int nbEmp, int nbApp){
        WSPModel m=new WSPModel();
        Job j;
        int k=0;
        while(k<nbJob){
            j=randomJob();
            if(m.getJobs().isEmpty()){//if first job
                m.getJobs().add(j);
                k++;
            }
            else {
                if (!m.getJobs().contains(j)&&j.getId()!=m.getJobs().get(m.getJobs().size()-1).getId()) {//we don't want to put the same job twice & not already this id
                    m.getJobs().add(j);
                    k++;
                }
            }
        }
        k=0;
        Employee emp;
        while(k<nbEmp){
            emp=randomEmp();
            if(m.getEmployees().isEmpty()) {//if first employee
                m.getEmployees().add(emp);
                k++;
            }
            else {
                if (!m.getEmployees().contains(emp)&&emp.getId()!=m.getEmployees().get(m.getEmployees().size()-1).getId()) {//we don't want to put the same employee twice and not already this id
                    m.getEmployees().add(emp);
                    k++;
                }
            }

        }
        k=0;
        Appointment app;
        int ne=randomInt(1,nbEmp);
        while(k<nbApp){
            app=randomApp(m,ne);
            if(!m.getAppointments().contains(app)){//we don't want to put the same appointment twice
                m.getAppointments().add(app);
                k++;
            }
        }
        return m;
    }

    public static WSPModel modelRandom(){
        WSPModel model = new WSPModel();
        //jobs
        int nbJobs=randomInt(1,10);
        int k=0;
        Job j;
        while(k<nbJobs){
            j=randomJob();
            if(!model.getJobs().contains(j)){
                model.getJobs().add(j);
                k++;
            }
        }
        //employees
        int nbEmp=randomInt(1,3);
        for(int i=0;i<nbEmp;i++){
            model.addEmployee(randomEmp());
        }
        //appointments
        int nbApp=randomInt(1,3);
        int ne=randomInt(1,nbEmp);
        for(int i=0;i<nbApp;i++){
            model.addAppointment(randomApp(model,ne));
        }
        return model;
    }

    public static int randomInt(int min,int max){//random between min and max
        return min+new Random().nextInt(max-min+1);
    }

    public static Skill randomSkill(int a,int b){
        int id1=randomInt(0,skillsEnum.values().length-1);
        skillsEnum name1= skillsEnum.values()[id1];
        int level1=randomInt(a,b);
        return new Skill(id1,name1.toString(),level1);
    }

    public static Affectation randomAffectation(){
        Job j= randomJob();
        TimeInterval w= randomTimeInterval();
        return new Affectation(j,w);
    }

    public static Appointment randomApp(WSPModel m,int ne){
        Job j= m.getJobs().get(randomInt(0,m.getJobs().size()-1));
        int nbEmp=randomInt(1,ne);
        List<Employee> n=new ArrayList<>();
        Employee e;
        int k=0;
        while(k<nbEmp){
            e=m.getEmployees().get(randomInt(0,m.getEmployees().size()-1));
            if(!n.contains(e)) {//we don't want an appointment with twice the same employee
                n.add(e);
                k++;
            }
        }
        return new Appointment(j,n);
    }

    public static Employee randomEmp(){
        int k;
        //id
        int id1=randomInt(1,1000);
        //shifts
        List<TimeInterval> standard = new ArrayList<>();//standard day
        standard.add(new TimeInterval(LocalTime.of(9,0),LocalTime.of(12,0)));//9h-12h
        standard.add(new TimeInterval(LocalTime.of(13,0),LocalTime.of(17,0)));//13h-17h
        //startingLoc
        Position start= randomPosition();
        //skills
        Skill s;
        int nbSkills=randomInt(1,10);
        List<Skill> sk=new ArrayList<>();
        k=0;
        while(k<nbSkills){
            s=randomSkill(1,10);
            if(!sk.contains(s)){//we don't want to put the same skill twice
                k++;
                sk.add(s);
            }
        }
        Employee emp=new Employee(id1,standard,start,sk);
        emp.setName(String.valueOf(id1));
        return emp;
    }

    public static Job randomJob(){
        int id=randomInt(0,999);
        Position location= randomPosition();
        TimeInterval openings= randomTimeInterval();

        LocalTime deadline= randomTime();
        List<Skill> skills=new ArrayList<>();
        int nbSkills=randomInt(1,2);
        int k=0;
        Skill s;
        boolean contains;
        while(k<nbSkills){
            s=randomSkill(1,5);
            contains=false;
            for(int i=0;i<skills.size();i++){
                if(skills.get(i).getName().equals(s.getName()))
                    contains=true;
            }
            if(!contains){
                skills.add(s);
                k++;
            }
        }
        Duration duration=randomDuration();
        Job job=new Job(id,location,openings,deadline,skills,duration);
        String desc="";
        for(int i=0;i<skills.size();i++) {
            if(i!=skills.size()-1)
                desc += skills.get(i).getName()+", ";
            else
                desc += skills.get(i).getName();
        }
        job.setDesc(desc);
        return job;
   }

   public static Duration randomDuration(){
        List<Duration> ld=new ArrayList<>();
        for(int i=0;i<3;i++){
            ld.add(Duration.ofMinutes(30));
        }
        for(int i=0;i<4;i++){
            ld.add(Duration.ofMinutes(45));
        }
        for(int i=0;i<4;i++){
            ld.add(Duration.ofMinutes(60));
        }
        for(int i=0;i<2;i++){
            ld.add(Duration.ofMinutes(90));
        }
        for(int i=0;i<1;i++){
           ld.add(Duration.ofMinutes(120));
        }
        int j=randomInt(0,ld.size()-1);
        return ld.get(j);
    }

    public static float randomFloat(int min,int max){
        return min+new Random().nextFloat()*(max-min);
    }

    public static Position randomPosition(){
        int id1=randomInt(0,9999);
        float lat=randomFloat(434,441)/10f;
        float lon=randomFloat(37,43)/10f;
        return new Position(id1,lat,lon);
    }

    public static LocalTime randomTime(){
        int h=randomInt(9,17);
        int m=randomInt(0,59);
        return LocalTime.of(h,m);
    }

    public static TimeInterval randomTimeInterval(){
        LocalTime startT;
        LocalTime endT;
        do {
            startT= LocalTime.of(randomInt(9,16),randomInt(0,59));
            endT= LocalTime.of(randomInt(9,17),randomInt(0,59));
        }while(endT.isBefore(startT) || endT.equals(startT));
        return new TimeInterval(startT,endT);
    }

    // Adds a TimeInterval object to a list of TimeIntervals if and only if it doesn't intersect with the elements of the list
    public static boolean addInterval(List<TimeInterval> li, TimeInterval t){

        boolean addImp = false;
        int i = 0;

        while(i < li.size() && !addImp) {
            addImp = li.get(i).intersect(t);
            i++;
        }
        if(!addImp){
            li.add(t);
        }
        return true;
    }
}
