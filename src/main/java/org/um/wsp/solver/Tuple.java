package org.um.wsp.solver;

public class Tuple {
    private int job;
    private int emp;
    private int app;

    public Tuple(int j,int e,int a){
        this.job=j;
        this.emp=e;
        this.app=a;
    }

    public int getJob() {
        return job;
    }

    public void setJob(int job) {
        this.job = job;
    }

    public int getEmp() {
        return emp;
    }

    public void setEmp(int emp) {
        this.emp = emp;
    }

    public int getApp() {
        return app;
    }

    public void setApp(int app) {
        this.app = app;
    }

    public String toString(){
        return "("+job+","+emp+","+app+")";
    }
}
