package org.um.wsp.solver;

import com.google.ortools.linearsolver.MPConstraint;
import com.google.ortools.linearsolver.MPObjective;
import com.google.ortools.linearsolver.MPSolver;
import com.google.ortools.linearsolver.MPVariable;
import com.skaggsm.ortools.OrToolsHelper;
import org.um.wsp.model.*;
import org.um.wsp.solution.Edt;
import org.um.wsp.solution.Solution;

import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class MIPSolver {
    private WSPModel model;
    public static final double ALPHA = 1.0;
    public static final double BETA = 1.0;
    public static final double GAMMA = 1.0;
    private MPVariable[][][] x;
    private MPVariable[][] s;
    private Duration[][] w;

    static {
        // Somewhere before using OR-Tools classes
        OrToolsHelper.loadLibrary();
    }
    public MIPSolver(){
        this.model = new WSPModel();
        this.x = null;
        this.s = null;
        this.w = null;

    }


    public WSPModel getModel() {
        return model;
    }
    public MPVariable[][][] getX(){return x;}
    public MPVariable[][] getS(){return s;}
    public Duration[][] getW(){return w;}

    public void setModel(WSPModel model) {
        this.model = model;
    }
    public void setX(MPVariable[][][] x) {
        this.x = x;
    }

    public void setS(MPVariable[][] s) {
        this.s = s;
    }

    public void setW(Duration[][] w) {
        this.w = w;
    }


    double dateToDouble(LocalTime t) {
        return t.getHour() +  ((t.getMinute() * 60 + (double)t.getSecond()) / 3600.0);
    }

    double durationToDouble(Duration d) {
        return (double)d.getSeconds()/3600.0 ;
    }

    MPSolver.ResultStatus mipWsrp( WSPModel model, long ms) {
        this.model = model;
        List<Employee> employees = model.getEmployees();
        int nbEmployees = employees.size();
        List<Job> jobs = model.getJobs();
        int nbJobs = jobs.size();
        double bigM = 12.0; // stands for 12h which will be outside the timeSchedule when called
        Duration[][] w = new Duration[nbJobs + 1][nbEmployees];//a job duration should be the same for every employee, thus we might not need a 2D-array here it is just to distinguish between the houses
        for (int i = 0; i < nbJobs; i++) {
            for (int j = 0; j < nbEmployees; j++) {
                w[i][j] = jobs.get(i).getDuration();
            }
        }
        // Houses
        for (int j = 0; j < nbEmployees; j++) {
            w[nbJobs][j] = Duration.ZERO;
        }
        this.setW(w);
        // Distances (Times are unneeded as long as the speed remains the same for every employee)
        double[][][] d = new double[nbEmployees][nbJobs + 1][nbJobs + 1];  //Need a 3D-array to not include the other houses for each employee
        for (int i = 0; i < d.length; i++) {
            for (int j = 0; j < d[i].length; j++) {
                for (int k = 0; k < d[i][j].length; k++) {
                    Position p1, p2;
                    if (j == (d[i].length - 1)) {
                        p1 = employees.get(i).getStartingLoc();
                    } else {
                        p1 = jobs.get(j).getLocation();
                    }
                    if (k == (d[i][j].length - 1)) {
                        p2 = employees.get(i).getStartingLoc();
                    } else {
                        p2 = jobs.get(k).getLocation();
                    }
                    d[i][j][k] = Position.distance(p1, p2);
                }
            }
        }
        Duration[][][] t = new Duration[nbEmployees][nbJobs + 1][nbJobs + 1];  //Need a 3D-array to not include the other houses for each employee
        for (int i = 0; i < t.length; i++) {
            for (int j = 0; j < t[i].length; j++) {
                for (int k = 0; k < t[i][j].length; k++) {
                    Position p1, p2;
                    if (j == (t[i].length - 1)) {
                        p1 = employees.get(i).getStartingLoc();
                    } else {
                        p1 = jobs.get(j).getLocation();
                    }
                    if (k == (t[i][j].length - 1)) {
                        p2 = employees.get(i).getStartingLoc();
                    } else {
                        p2 = jobs.get(k).getLocation();
                    }
                    t[i][j][k] = Position.travelTime(p1, p2);
                }
            }
        }

       /* MPSolver solver =
                new MPSolver("MIPWSRP", MPSolver.OptimizationProblemType.CBC_MIXED_INTEGER_PROGRAMMING); */
        MPSolver solver = MPSolver.createSolver("SCIP");

      //solver.enableOutput();


        //Decision Variables

        //xi_j_k exists iff job i & job j can be affected to employee k the last job in each dimension (for jobs) is the employee's home location
        MPVariable[][][] x = new MPVariable[nbJobs + 1][nbJobs + 1][nbEmployees];
        for (int i = 0; i < nbJobs + 1; i++) {
            for (int j = 0; j < nbJobs + 1; j++) {
                for (int k = 0; k < nbEmployees; k++)
                    if ((i == nbJobs) || (j == nbJobs) || (Compatibility.canDo(employees.get(k), jobs.get(i)) && Compatibility.canDo(employees.get(k), jobs.get(j)))) {
                        if (i == nbJobs && j != nbJobs) {
                            if (Compatibility.canDo(employees.get(k), jobs.get(j))) {
                                x[i][j][k] = solver.makeBoolVar("x" + i + "_" + j + "_" + k);
                            } else {
                                x[i][j][k] = solver.makeNumVar(0.0, 0.0, "x" + i + "_" + j + "_" + k);
                            }
                        }
                        else if(i!= nbJobs && j == nbJobs){
                                if (Compatibility.canDo(employees.get(k), jobs.get(i))) {
                                    x[i][j][k] = solver.makeBoolVar("x" + i + "_" + j + "_" + k);
                                }
                                else{
                                    x[i][j][k] = solver.makeNumVar(0.0, 0.0, "x" + i + "_" + j + "_" + k);
                                }

                        }
                        else {
                            x[i][j][k] = solver.makeBoolVar("x" + i + "_" + j + "_" + k);
                        }

                        }
                else {
                            x[i][j][k] = solver.makeNumVar(0.0, 0.0, "x" + i + "_" + j + "_" + k);
                        }
                    }
            }
            this.setX(x);

            //si_k is the starting date for the task k done by employee i
            MPVariable[][] s = new MPVariable[nbEmployees][nbJobs + 1];
            for (int i = 0; i < s.length; i++) {
                for (int k = 0; k < s[i].length; k++) {
                    s[i][k] = solver.makeNumVar(9.0, 17.0, "s" + i + "_" + k);
                }
            }

            this.setS(s);

            //pk_i allows an employee i to do the job k after the break
            MPVariable[][] p = new MPVariable[nbEmployees][nbJobs + 1];
            for (int i = 0; i < p.length; i++) {
                for (int k = 0; k < p[i].length; k++) {
                    p[i][k] = solver.makeBoolVar("p" + i + "_" + k);
                }
            }
            // Constraints

            // A job has to have at least the number of required employees (here we haven't checked the case of houses since it's done in constraint (4))
            //WARNING Relaxed to let jobs being unassigned
      /*  MPConstraint[] tabC = new MPConstraint[nbJobs + 1]; // 1 constraint per job
        for (int i = 0; i < tabC.length; i++) {
            if (i == nbJobs) {
                tabC[i] = solver.makeConstraint(1.0, 1.0, "c" + i);
            } else {
                tabC[i] = solver.makeConstraint(jobs.get(i).getNbEmpRequired(), Double.POSITIVE_INFINITY, "c" + i);
            }
            for (int j = 0; j < x.length; j++) {
                for (int k = 0; k < x[j][i].length; k++) {
                    if (x[j][i][k] != null) {
                        tabC[i].setCoefficient(x[j][i][k], 1.0);
                    }
                }
            }
        }
*/

            // Computing the "delta_i_k" which represents the overtime (can be negative) (i.e. delta_i_k - s_i_k = w_i_k - due_i)
            MPConstraint[][] tabDelta = new MPConstraint[nbJobs + 1][nbEmployees];
            MPVariable[][] delta = new MPVariable[nbJobs + 1][nbEmployees];
            for (int i = 0; i < delta.length; i++) {
                for (int j = 0; j < delta[i].length; j++) {
                    delta[i][j] = solver.makeNumVar(-17.0, 17.0, "delta" + i + "_" + j);
                }
            }
            for (int i = 0; i < tabDelta.length; i++) {
                for (int j = 0; j < tabDelta[i].length; j++) {
                    if (i < nbJobs) {
                        tabDelta[i][j] = solver.makeConstraint(durationToDouble(w[i][j]) - dateToDouble(jobs.get(i).getDeadline()), durationToDouble(w[i][j]) - dateToDouble(jobs.get(i).getDeadline()), "d" + i + "_" + j);
                        tabDelta[i][j].setCoefficient(delta[i][j], 1.0);
                        tabDelta[i][j].setCoefficient(s[j][i], -1.0);
                    } else { // No meaning to define a deadline to be at home apart from scheduling constraints
                        tabDelta[i][j] = solver.makeConstraint(0.0, 0.0, "d" + i + "_" + j);
                        tabDelta[i][j].setCoefficient(delta[i][j], 1.0);
                    }


                }
            }

            // The objective needs "delta_i_k" iff it is positive it will be r_i_j
            MPConstraint[][] tabDeltaBis = new MPConstraint[nbJobs + 1][nbEmployees];
            MPVariable[][] r = new MPVariable[nbJobs + 1][nbEmployees];// will contain the positive part of delta
            MPVariable[][] rBis = new MPVariable[nbJobs + 1][nbEmployees]; // will contain the negative part of delta
            for (int i = 0; i < r.length; i++) {
                for (int j = 0; j < r[i].length; j++) {
                    r[i][j] = solver.makeNumVar(0.0, Double.POSITIVE_INFINITY, "rpos" + i + "_" + j);
                    rBis[i][j] = solver.makeNumVar(0.0, Double.POSITIVE_INFINITY, "rneg" + i + "_" + j);
                }
            }
            for (int i = 0; i < tabDeltaBis.length; i++) {
                for (int j = 0; j < tabDeltaBis[i].length; j++) {

                    tabDeltaBis[i][j] = solver.makeConstraint(0.0, 0.0, "r" + i + "_" + j);
                    tabDeltaBis[i][j].setCoefficient(delta[i][j], 1.0);
                    tabDeltaBis[i][j].setCoefficient(r[i][j], -1.0);
                    tabDeltaBis[i][j].setCoefficient(rBis[i][j], 1.0);


                }
            }

            // for all job i for all employee k sum(x_i_j_k) <= 1
            MPConstraint[][] tabG_ter = new MPConstraint[nbJobs + 1][nbEmployees];
            for (int i = 0; i < tabG_ter.length; i++) {
                for (int j = 0; j < tabG_ter[i].length; j++) {
                    if (i < nbJobs) {
                        tabG_ter[i][j] = solver.makeConstraint(Double.NEGATIVE_INFINITY, 1.0, "g_ter_" + i + "_" + j);
                    } else { //forced to go outside our house (maybe to go to our house if unassigned)
                        tabG_ter[i][j] = solver.makeConstraint(1.0, 1.0, "g_ter_" + i + "_" + j);
                    }
                    for (int k = 0; k < nbJobs + 1; k++) {
                        tabG_ter[i][j].setCoefficient(x[i][k][j], 1.0);
                    }
                }
            }
            // for all job j for all employee k sum(x_i_j_k) <= 1
            MPConstraint[][] tabG_terbis = new MPConstraint[nbJobs + 1][nbEmployees];
            for (int i = 0; i < tabG_terbis.length; i++) {
                for (int j = 0; j < tabG_terbis[i].length; j++) {
                    if (i < nbJobs) {
                        tabG_terbis[i][j] = solver.makeConstraint(Double.NEGATIVE_INFINITY, 1.0, "g_terbis_" + i + "_" + j);
                    } else { //forced to go back to our house (maybe coming from our house if unassigned)
                        tabG_terbis[i][j] = solver.makeConstraint(1.0, 1.0, "g_terbis_" + i + "_" + j);
                    }
                    for (int k = 0; k < nbJobs + 1; k++) {
                        tabG_terbis[i][j].setCoefficient(x[k][i][j], 1.0);
                    }
                }
            }
            // for all i, for all k sum(x_i_j_k) = sum(x_j_i_k)
            MPConstraint[][] tabG_terter = new MPConstraint[nbJobs + 1][nbEmployees];
            for (int i = 0; i < tabG_terter.length; i++) {
                for (int j = 0; j < tabG_terter[i].length; j++) {
                    tabG_terter[i][j] = solver.makeConstraint(0.0, 0.0, "g_terter" + i + "_" + j);
                    for (int k = 0; k < nbJobs + 1; k++) {
                        tabG_terter[i][j].setCoefficient(x[i][k][j], 1.0);
                        tabG_terter[i][j].setCoefficient(x[k][i][j], -1.0);
                    }
                }
            }
            // for all i x_i_i_k  = 0 Except for houses which means unassigned employee
            MPConstraint[][] tabG_prime = new MPConstraint[nbJobs][nbEmployees];

            for (int i = 0; i < tabG_prime.length; i++) {
                for (int j = 0; j < tabG_prime[i].length; j++) {
                    tabG_prime[i][j] = solver.makeConstraint(0, 0, "g_prime" + i + "_" + j);
                    tabG_prime[i][j].setCoefficient(x[i][i][j], 1.0);
                }
            }
            //for all i,j x_i_j_k + x_j_i_k <= 1
            MPConstraint[][] tabG_second = new MPConstraint[nbJobs][nbJobs];

            for (int i = 0; i < tabG_second.length; i++) {
                for (int j = 0; j < tabG_second[i].length; j++) {
                    for (int k = 0; k < nbEmployees; k++) {
                        tabG_second[i][j] = solver.makeConstraint(Double.NEGATIVE_INFINITY, 1.0, "g_second" + i + "_" + j);
                        tabG_second[i][j].setCoefficient(x[i][j][k], 1.0);
                        tabG_second[i][j].setCoefficient(x[j][i][k], 1.0);
                    }
                }
            }

            // for all i sum(xijk) <= 1
            MPConstraint[] tabG_last = new MPConstraint[nbJobs];

            for (int i = 0; i < tabG_last.length; i++) {
                tabG_last[i] = solver.makeConstraint(Double.NEGATIVE_INFINITY, 1.0, "g_last" + i);
                for (int j = 0; j < nbJobs + 1; j++) {
                    for (int k = 0; k < nbEmployees; k++) {
                        tabG_last[i].setCoefficient(x[i][j][k], 1.0);
                    }
                }
            }
            // for all i sum(xjik) <= 1
            MPConstraint[] tabG_lastbis = new MPConstraint[nbJobs];

            for (int i = 0; i < tabG_lastbis.length; i++) {
                tabG_lastbis[i] = solver.makeConstraint(Double.NEGATIVE_INFINITY, 1.0, "g_lastbis" + i);
                for (int j = 0; j < nbJobs + 1; j++) {
                    for (int k = 0; k < nbEmployees; k++) {
                        tabG_lastbis[i].setCoefficient(x[j][i][k], 1.0);
                    }
                }
            }


            MPConstraint[] tab_pause_h = new MPConstraint[nbEmployees];
            for (int i = 0; i < tab_pause_h.length; i++) {
                tab_pause_h[i] = solver.makeConstraint(0.0, 0.0, "pause_h" + i);
                tab_pause_h[i].setCoefficient(p[i][nbJobs], 1.0);
            }
            MPConstraint[][] tab_shift_0_d = new MPConstraint[nbEmployees][nbJobs + 1];

            for (int i = 0; i < tab_shift_0_d.length; i++) {
                for (int j = 0; j < tab_shift_0_d[i].length; j++) {
                    tab_shift_0_d[i][j] = solver.makeConstraint(dateToDouble(employees.get(i).getShifts().get(0).getStart()), Double.POSITIVE_INFINITY, "sh_" + i + "_" + j);
                    tab_shift_0_d[i][j].setCoefficient(s[i][j], 1.0);
                    tab_shift_0_d[i][j].setCoefficient(p[i][j], bigM);
                }
            }
            MPConstraint[][] tab_shift_0_f = new MPConstraint[nbEmployees][nbJobs + 1];

            for (int i = 0; i < tab_shift_0_f.length; i++) {
                for (int j = 0; j < tab_shift_0_f[i].length; j++) {
                    tab_shift_0_f[i][j] = solver.makeConstraint(Double.NEGATIVE_INFINITY, dateToDouble(employees.get(i).getShifts().get(0).getEnd()) - durationToDouble(w[j][i]), "shf_" + i + "_" + j);
                    tab_shift_0_f[i][j].setCoefficient(s[i][j], 1.0);
                    tab_shift_0_f[i][j].setCoefficient(p[i][j], -bigM);
                }
            }
            MPConstraint[][] tab_shift_1_d = new MPConstraint[nbEmployees][nbJobs + 1];

            for (int i = 0; i < tab_shift_1_d.length; i++) {
                for (int j = 0; j < tab_shift_1_d[i].length; j++) {
                    tab_shift_1_d[i][j] = solver.makeConstraint(dateToDouble(employees.get(i).getShifts().get(1).getStart()) - bigM, Double.POSITIVE_INFINITY, "sh_b" + i + "_" + j);
                    tab_shift_1_d[i][j].setCoefficient(s[i][j], 1.0);
                    tab_shift_1_d[i][j].setCoefficient(p[i][j], -bigM);
                }
            }
            MPConstraint[][] tab_shift_1_f = new MPConstraint[nbEmployees][nbJobs + 1];

            for (int i = 0; i < tab_shift_1_f.length; i++) {
                for (int j = 0; j < tab_shift_1_f[i].length; j++) {
                    tab_shift_1_f[i][j] = solver.makeConstraint(Double.NEGATIVE_INFINITY, dateToDouble(employees.get(i).getShifts().get(1).getEnd()) + bigM - durationToDouble(w[j][i]), "shbf_" + i + "_" + j);
                    tab_shift_1_f[i][j].setCoefficient(s[i][j], 1.0);
                    tab_shift_1_f[i][j].setCoefficient(p[i][j], bigM);
                }
            }
            //Constraint (3)  it was non-linear so I have rewritten it : s_i_k - s_j_k + x_i_j_k (w_i_k + t_i_j + l-e) <= (l-e)
            MPConstraint[][][] tabE = new MPConstraint[nbJobs + 1][nbJobs][nbEmployees];
            for (int i = 0; i < tabE.length; i++) {
                for (int j = 0; j < tabE[i].length; j++) {
                    for (int k = 0; k < tabE[i][j].length; k++) {
                        if (i != j) { // To avoid having duplicates
                            tabE[i][j][k] = solver.makeConstraint(Double.NEGATIVE_INFINITY, 8.0, "e" + i + "_" + j + "_" + k); // 8.0 is (l-e)
                            tabE[i][j][k].setCoefficient(s[k][i], 1.0);
                            tabE[i][j][k].setCoefficient(s[k][j], -1.0);
                            tabE[i][j][k].setCoefficient(x[i][j][k], durationToDouble(w[i][k]) + (d[k][i][j] / (60.0 * Position.SPEED) + 0.0003) + 8.0); //0.0003 stands for 1 second


                        }
                    }
                }
            }

 /*      // Constraint (4)
        MPConstraint[] tabG = new MPConstraint[nbEmployees];
        for (int i = 0; i < tabG.length; i++) {
            tabG[i] = solver.makeConstraint(2.0, 2.0, "g" + i);
        }

        for (int k = 0; k < nbEmployees; k++) {
            for (int i = 0; i < nbJobs + 1; i++) {
                tabG[k].setCoefficient(x[i][nbJobs][k], 1.0);
                tabG[k].setCoefficient(x[nbJobs][i][k], 1.0);
            }
        }
*/

/*       Inutile car j'ai restreint le domaine // Constraint (5)
        MPConstraint[] tabH = new MPConstraint[nbEmployees];
        for (int i = 0; i < tabH.length; i++) {
            tabH[i] = solver.makeConstraint(9.0, Double.POSITIVE_INFINITY, "h" + i); // 9.0 has to be replaced by e
        }

        for (int k = 0; k < nbEmployees; k++) {
            tabH[k].setCoefficient(s[k][nbJobs], 1.0);
        }
        */

            //Constraint (5') s_hk_k - s_i_j <= 0
            MPConstraint[][] tabI = new MPConstraint[nbEmployees][nbJobs];
            for (int i = 0; i < tabI.length - 1; i++) {
                for (int j = 0; j < tabI[i].length; j++) {
                    tabI[i][j] = solver.makeConstraint(Double.NEGATIVE_INFINITY, 0.0, "i" + i + "_" + j);
                    tabI[i][j].setCoefficient(s[i][nbJobs], 1.0);
                    tabI[i][j].setCoefficient(s[i][j], -1.0);
                }
            }


 /*       //Constraint (6) s_i_k  <= l - w_i_k - t_i_hk
        MPConstraint[][] tabJ = new MPConstraint[nbJobs + 1][nbEmployees];
        for (int i = 0; i < tabJ.length; i++) {
            for (int j = 0; j < tabJ[i].length; j++) {
                tabJ[i][j] = solver.makeConstraint(Double.NEGATIVE_INFINITY, 17.0 - durationToDouble(w[i][j]) - (d[j][i][nbJobs] / (60.0* Position.SPEED)), "j" + i + "_" + j);
                tabJ[i][j].setCoefficient(s[j][i], 1.0);
            }
        }
*/

            // Objective function
            MPObjective objective = solver.objective();
            for (int i = 0; i < (nbJobs + 1); i++) {
                for (int j = 0; j < (nbJobs + 1); j++) {
                    for (int k = 0; k < nbEmployees; k++) {
                        objective.setCoefficient(x[i][j][k], ALPHA * d[k][i][j]);
                        objective.setCoefficient(x[i][j][k], -1.0 * BETA * durationToDouble(w[i][k]));
                        objective.setCoefficient(r[i][k], GAMMA);
                        //                   objective.setCoefficient(x[i][j][k],1.0);
                    }
                }
            }
            objective.setMinimization();
            solver.setTimeLimit(ms);
    //       String lpmodel = solver.exportModelAsLpFormat();
      //    System.out.println(lpmodel);
       /* this.setX(x);
        this.setS(s);*/

            MPSolver.ResultStatus resultStatus = solver.solve();
            //assertEquals(MPSolver.ResultStatus.OPTIMAL, resultStatus); //test unitaire
            if (resultStatus == MPSolver.ResultStatus.OPTIMAL) {
                System.out.println("Solution:");
                //System.out.format("Objective value = %dn", (double)objective.value());
/*                System.out.println("Shifts Emploués");
                for (int i = 0; i < nbEmployees; i++) {
                    for (int j = 0; j < employees.get(i).getShifts().size(); j++) {
                        System.out.println(employees.get(i).getShifts().get(j).getStart());
                        System.out.println(employees.get(i).getShifts().get(j).getEnd());
                    }

                }
                System.out.println("Tableau x");
                for (int i = 0; i < nbEmployees; i++) {
                    for (int j = 0; j < nbJobs + 1; j++) {
                        for (int k = 0; k < nbJobs + 1; k++) {
                            if (x[k][j][i] != null) {
                                System.out.println("x_" + k + "_" + j + "_" + i + "= " + x[k][j][i].solutionValue());
                            }
                        }
                    }
                }
                System.out.println("Tableau s");
                for (int i = 0; i < getS().length; i++) {
                    for (int j = 0; j < getS()[i].length; j++) {
                        System.out.println("s_" + i + "_" + j + "= " + s[i][j].solutionValue());
                    }
                }
                System.out.println("Tableau de retard");
/*            for(int i = 0; i < r.length; i++){
                for(int j = 0; j < r[i].length; j++){
                    System.out.println("r_"+ i +"_" + j+ "= " + r[i][j].solutionValue());
                }
            }
            System.out.println("Tableau de retard négatif");
            for(int i = 0; i < rBis.length; i++){
                for(int j = 0; j < rBis[i].length; j++){
                    System.out.println("rbis_"+ i +"_" + j+ "= " + rBis[i][j].solutionValue());
                }
            }
            System.out.println("Tableau de delta");
            for(int i = 0; i < delta.length; i++){
                for(int j = 0; j < delta[i].length; j++){
                    System.out.println("delta_"+ i +"_" + j+ "= " + delta[i][j].solutionValue());
                }
            }

 */
  /*              for (int i = 0; i < nbEmployees; i++) {
                    System.out.println("Time Schedule for Employee " + employees.get(i).getName());
                    int nextTask = nbJobs;
                    boolean done = false;
                    while (!done) {
                        boolean nextTaskFound = false;
                        int cpt = 0;
                        while (cpt <= nbJobs && !(nextTaskFound)) {
                            if (x[nextTask][cpt][i].solutionValue() == 1) {
                                if (nextTask != nbJobs) {
                                    System.out.println("Job : " + jobs.get(nextTask).getDesc());
                                    System.out.println("Start :" + LocalTime.ofSecondOfDay((long) Math.ceil(s[i][nextTask].solutionValue() * 3600)));
                                    nextTask = cpt;
                                    System.out.println("next Task is :" + nextTask);
                                    nextTaskFound = true;
                                    if (cpt == nbJobs) {
                                        done = true;
                                    }

                                } else {
                                    if (nextTask == nbJobs) {
                                        System.out.println("Job : House");
                                        System.out.println("Start :" + LocalTime.ofSecondOfDay((long) Math.ceil(s[i][nextTask].solutionValue() * 3600)));
                                        nextTask = cpt;
                                        if (cpt == nbJobs) {
                                            done = true;
                                        }
                                    }
                                }
                            } else {
                                System.out.println("et non");
                                done = true;
                            }
                            cpt++;
                        }


                    }
                }*/
                System.out.println("\nAdvanced usage:");
                System.out.format("Problem solved in %d milliseconds %n", solver.wallTime());
                System.out.format("Problem solved in %d iterations %n", solver.iterations());
                System.out.format("Problem solved in %d branch-and-bound nodes %n", solver.nodes());
            } else {
                System.out.println("The problem does not have an optimal solution!");
            }
            return resultStatus;
        }

    public Solution solveMIP(WSPModel model, long ms){ //TODO Duration au lieu de long
        Solution s = new Solution();
        this.model = model;
        if(this.mipWsrp(model, ms) == MPSolver.ResultStatus.FEASIBLE || this.mipWsrp(model, ms) == MPSolver.ResultStatus.OPTIMAL) {
            // ça devrait tourner que si j'ai une solution
            // Générer la liste d'Edt qui a besoin d'un employé et d'une liste d'affectation
            List<Employee> lemp = model.getEmployees();
            List<Job> ljob = model.getJobs();
            List<Edt> ledt = new ArrayList<>();
            for (int i = 0; i < lemp.size(); i++) {
                //Construire la liste d'affectation constituer d'un job et d'une timeWindow besoin de s pour le moment
                List<Affectation> laff = new ArrayList<>();
                for (int j = 0; j < ljob.size() + 1; j++) {
                    //Penser à optimiser (notamment cas j = j_prime) genre un while aurait peut-être été plus judicieux
                    for (int j_prime = 0; j_prime < ljob.size() + 1; j_prime++) {
                        if (this.x[j][j_prime][i].solutionValue() == 1) {
                            LocalTime start = LocalTime.ofSecondOfDay((long) Math.ceil(this.s[i][j].solutionValue() * 3600));
                            LocalTime end = LocalTime.ofSecondOfDay((long) Math.ceil((this.s[i][j].solutionValue() + durationToDouble(this.w[j][i])) * 3600));
                            TimeInterval window = new TimeInterval(start, end);
                            if(j != ljob.size()) {
                                Affectation a = new Affectation(ljob.get(j), window);
                                laff.add(a);
                            }
                            else{
                                Affectation a_home = new Affectation(new Job(-1, lemp.get(i).getStartingLoc()),window);
                            }

                        }
                    }

                }
                ledt.add(new Edt(lemp.get(i), laff));

                //Unassigned jobs
                List<Job> unassigned = new ArrayList<>();
                for (int k = 0; k < ljob.size() + 1; k++) {
                    int cpt = 0;
                    for (int l = 0; l < ljob.size() + 1; l++) {
                        if (this.x[k][l][i].solutionValue() == 1) {
                            cpt++;
                        }
                    }

                    if (cpt == 0) {
                        unassigned.add(ljob.get(i));
                    }
                }
                s.setListEdt(ledt);
                s.setUnassigned(unassigned);

            }
        }

        return s;
/*System.out.println("Employés :");
            for(int i = 0; i < lemp.size(); i++){
                System.out.println(lemp.get(i).getName());
            }

            System.out.println("Jobs :");
            for(int i = 0; i < ljob.size(); i++){
                System.out.println(ljob.get(i).getDesc());
            }*/

    }
}