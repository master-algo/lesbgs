package org.um.wsp.solver;

import org.um.wsp.model.*;
import org.um.wsp.solution.Solution;
import java.util.Comparator;
import java.util.List;

public class HeuristicJojo {
    private HeuristicJojo(){}

    public static Solution solve(WSPModel model,String s){
        sort(model,s);
        HeuristicSolver h= new HeuristicSolver(model);
        return h.solve();
    }

    public static void sort(WSPModel model,String s){
        if(s.equals("Duration"))
            sortDuration(model.getJobs());
        if(s.equals("Skill"))
            sortSkill(model.getJobs());
        if(s.equals("Deadline"))
            sortDeadline(model.getJobs());
    }

    public static void sortDuration(List<Job> lj){
        lj.sort(Comparator.comparing(Job::getDuration));
    }

    public static void sortSkill(List<Job> lj){
        lj.sort((o1, o2) -> {
            int res = Integer.compare(o1.getSkills().size(), o2.getSkills().size());
            if(res==0){
                int sum1 = o1.getSkills().stream().mapToInt(Skill::getLevel).sum();
                int sum2 = o2.getSkills().stream().mapToInt(Skill::getLevel).sum();
                res = Integer.compare(sum1, sum2);
            }
            return -res;
        });
    }

    public static void sortDeadline(List<Job> lj){
        lj.sort(Comparator.comparing((Job::getDeadline)));
    }
}
