package org.um.wsp.solver;

import org.um.wsp.model.WSPModel;
import org.um.wsp.solution.KPI;
import org.um.wsp.solution.Solution;

import java.io.File;
import java.io.IOException;

public class Heuristic {
    private Heuristic(){}

    public static String name(int nbJob, int nbEmp, int nbApp){
        return nbJob+"-"+nbEmp+"-"+nbApp;
    }

    public static Solution heuristic(int nbJob,int nbEmp,int nbApp,String st,boolean test,String h) throws IOException {
        Solution s;
        if (h.equals("J")) {
            s = heuristicJojo(nbJob, nbEmp, nbApp, st,test);
        }
        else {
            s = heuristicSolver(nbJob, nbEmp, nbApp,test);
        }
        return s;
    }

    public static Solution heuristicJojo(int nbJob,int nbEmp, int nbApp,String st,boolean test) throws IOException {
        String endName=name(nbJob,nbEmp,nbApp);
        String nameFile="instanceRand-"+endName;
        String json=".json";
        Solution s;
        WSPModel m=new WSPModel().getFromJSON(nameFile+json);
        //solve
        s=HeuristicJojo.solve(m,st);
        //write
        File f=new File(st+"-testHeuristicJojoRand-"+endName+json);
        s.writeSolution(f);
        //print
        if(test) {
            System.out.println(nameFile);
            s.printSolution();
        }
        printKPIs(s, nameFile,test);
        return s;
    }

    public static Solution heuristicSolver(int nbJob,int nbEmp, int nbApp,boolean test)throws IOException{
        String endName=name(nbJob,nbEmp,nbApp);
        String nameFile="instanceRand-"+endName;
        String json=".json";
        Solution s;
        WSPModel m=new WSPModel().getFromJSON(nameFile+json);
        //solve
        HeuristicSolver h= new HeuristicSolver(m);
        s=h.solve();
        //to json
        File f=new File("testHeuristicTrisRand-"+endName+json);
        s.writeSolution(f);
        //print
        if(test) {
            System.out.println(nameFile);
            s.printSolution();
        }
        printKPIs(s, nameFile,test);
        return s;
    }

    public static void printKPIs(Solution s,String n,boolean test){
        KPI kpi = new KPI(s);
        if (test) {
            System.out.println("KPI : ");
            System.out.println("KPI RatioJob : " + kpi.getKPIRatioJob());
            System.out.println("KPI RatioUnassigned : " + kpi.getKPIRatioUnassigned());
            System.out.println("KPI AvgWorkingTime : " + kpi.getKPIAvgWorkingTime().toString());
            System.out.println("KPI AvgDrivingTime : " + kpi.getKPIAvgDrivingTime().toString());
            System.out.println("KPI AvgIdleTime : " + kpi.getKPIAvgIdleTime().toString());
        }
        System.out.print(n + " & "+kpi.getKPIRatioJob()+" & "+kpi.getKPIRatioUnassigned()+" & "+kpi.getKPIAvgWorkingTime().toString()+" & "+kpi.getKPIAvgDrivingTime().toString()+" & "+kpi.getKPIAvgIdleTime().toString());

    }
}
