package org.um.wsp.solver;

import org.um.wsp.model.*;
import org.um.wsp.solution.Edt;

import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;

public class Compatibility {
    private Compatibility(){
    }

    public static boolean canDo(Employee e, Job j){//true if Employee e is qualified enough to do Job j
        boolean b = true;
        int i = 0;
        ArrayList<Skill> list = (ArrayList<Skill>) j.getSkills();
        ArrayList<Skill> l = (ArrayList<Skill>) e.getSkills();
        while(i<list.size()&&b){//for all skills required for the job
            Skill skill = list.get(i);//a job's skill
            boolean a = false;
            for(Skill s : l){//an employee's skill
                if(s.getId()==skill.getId() && s.getLevel()>= skill.getLevel()){//checks if employee has the skill and the sufficient level
                    a = true;
                }
            }
            b = a;
            i++;
        }
        return b;
    }

    public static boolean canDoAfter(Employee e,Job j,Affectation i){//true if Employee e is qualified enough to do Job j, after affectation i
        LocalTime minStart = i.getJob().getOpenings().getEnd().plus(
                Position.travelTime(i.getJob().getLocation(), j.getLocation())
        );//accounting for travel time
        return canDo(e,j) && minStart.isBefore(j.getOpenings().getEnd().plus(j.getDuration()));
    }

    public static boolean canAdd(Edt edt, Job j, int shiftid){//true if Job j can be added in edt, accounting for travel time
        boolean b = true;
        Employee e = edt.getEmp();
        if(!e.getShifts().isEmpty() && !edt.getPlanning().isEmpty()) {
            TimeInterval ti = e.getShifts().get(shiftid);
            LocalTime end = ti.getEnd();//end of i-th shift of Employee e
            Affectation previous = edt.getPlanning().get(edt.getPlanning().size() - 1);
            LocalTime canStart = previous.getWindow().getEnd();
            Position startPos = previous.getJob().getLocation();
            LocalTime travel = canStart.plus(Position.travelTime(startPos, j.getLocation()));//first available time to start a job after travelling
            if ((j.getDuration().compareTo(new TimeInterval(travel, end).length()) > 0)/*&&(Position.travelTime(startPos, j.getLocation()).compareTo(Duration.ofHours(7))<0)&&(travel.isBefore(end)))*/) {//checks if job duration fits in the time window, accounting for travel time
                b = false;
            }
        }
        return b;
    }
}
