package org.um.wsp.solver;

import org.um.wsp.solution.*;
import org.um.wsp.model.*;

import java.time.Duration;
import java.time.LocalTime;
import java.util.*;

public class HeuristicSolver {
    private final WSPModel model;

    public HeuristicSolver(WSPModel model) {
        this.model = model;
    }

    public Solution solve(){//greedy heuristic
        Solution sol = new Solution();//creates empty solution
        ArrayList<Employee> listE = (ArrayList<Employee>) model.getEmployees();
        ArrayList<Job> listJ = (ArrayList<Job>) model.getJobs();
        ArrayList<Edt> lEdt=new ArrayList<>();
        for(Employee e : listE){//for each Employee e
            Edt edt = new Edt(e, new ArrayList<>());
            Job home=new Job(-1, e.getStartingLoc());
            home.setPause(true);
            Job pause;
            if(home.getLocation().getName()!=null)
                home.setDesc("Home : "+home.getLocation().getName());
            else
                home.setDesc("Home");
            for(TimeInterval ti : e.getShifts()){
                if(e.getShifts().indexOf(ti)==0 || edt.getPlanning().isEmpty()){
                    edt.getPlanning().add(new Affectation(home, new TimeInterval(ti.getStart(),ti.getStart())));//set starting loc in edt
                    //adds starting location of the day as first job with id 0, so that it appears in Edt
                }
                else{
                    pause= new Job(edt.getPlanning().get(edt.getPlanning().size()-1).getJob());
                    pause.setId(-1);
                    pause.setDesc("Pause");
                    pause.setPause(true);
                    edt.getPlanning().add(new Affectation(pause, new TimeInterval(edt.getPlanning().get(edt.getPlanning().size()-1).getWindow().getEnd().plus(Duration.ofMinutes(1)),ti.getStart())));//set starting loc in edt
                    //adds starting location of the current shift first job with id 0, so that it appears in Edt
                }
                int jj = 0;
                while(jj<listJ.size()&&!listJ.isEmpty()){
                    Job j=listJ.get(jj);
                    if(Compatibility.canDo(e,j) && Compatibility.canAdd(edt,j,e.getShifts().indexOf(ti))){//if e can fit j in his edt, then it's added
                        //canAdd code :
                        Affectation previous = edt.getPlanning().get(edt.getPlanning().size() - 1);
                        LocalTime canStart = previous.getWindow().getEnd();
                        Position startPos = previous.getJob().getLocation();
                        Duration add = Position.travelTime(startPos,j.getLocation());
                        edt.addDriving(add);
                        LocalTime travel = canStart.plus(add);

                        edt.getPlanning().add(new Affectation(j, new TimeInterval(travel, travel.plus(j.getDuration()))));//(added here)
                        listJ.remove(j);
                    }
                    else{
                        jj++;
                    }
                }
            }

            lEdt.add(edt);
        }
        sol.setListEdt(lEdt);
        sol.setUnassigned(listJ);
        return sol;
    }

}
