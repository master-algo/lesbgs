package org.um.wsp.solution;

import org.um.wsp.model.Affectation;
import org.um.wsp.model.Employee;
import org.um.wsp.model.Job;
import org.um.wsp.solver.Compatibility;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class Edt {
    private Employee emp;
    private List<Affectation> planning;
    private Duration driving=Duration.ZERO;

    public Edt(){
        this.emp=new Employee();
        this.planning=new ArrayList<>();
    }

    public Edt(Employee e,List<Affectation> l){
        this.emp=e;
        this.planning=l;
    }

    //Getters & Setters

    public List<Affectation> getPlanning() {
        return planning;
    }

    public void setPlanning(List<Affectation> planning) {
        this.planning = planning;
    }

    public Employee getEmp() {
        return emp;
    }

    public void setEmp(Employee emp) {
        this.emp = emp;
    }

    public Duration getDriving() {
        return driving;
    }

    public void setDriving(Duration driving) {
        this.driving = driving;
    }

    public void addDriving(Duration d){
        this.driving=this.driving.plus(d);
    }

    public List<Affectation> swapAffectation(int j,int k,List<Affectation> l){
        List<Affectation> lr=new ArrayList<>();
        Affectation a=l.get(j);
        Affectation b=l.get(k);
        for(int i=0;i<l.size();i++){
            if(i==j){
                lr.add(b);
            }
            else{
                if(i==k){
                    lr.add(a);
                }
                else{
                    lr.add(l.get(i));
                }
            }
        }
        return lr;
    }

    //validity checked
    public boolean sortAffectation(){//bubble sort
        boolean change=false;
        for(int i=planning.size()-1;i>0;i--){
            for(int j=0;j<i;j++){
                if(planning.get(j+1).getWindow().isBefore(planning.get(j).getWindow())){
                    planning=swapAffectation(j,j+1,planning);
                    change=true;
                }
            }
        }
        return change;
    }

    //TODO check if the employee have time to travel to a job to the following
    public boolean isValid(){
        boolean isValid=true;
        boolean intersect;
        for(int i=0;i<planning.size();i++){
            for (int j=i+1;j<planning.size();j++){//checks for every affectation after i
                intersect=planning.get(i).getWindow().intersect(planning.get(j).getWindow());
                isValid = isValid && !intersect;
            }
            Job j = planning.get(i).getJob();
            isValid = isValid && Compatibility.canDo(this.getEmp(),j);//qualified enough
        }//no jobs at the same moment
        return isValid;
    }

    public void printEdt(){
        System.out.println("Employee's id : "+emp.getId()+" "+emp.getName());
        for(int i=0;i<(int)planning.size();i++)
            if(i<planning.size()-1 || planning.get(i).getJob().getId()!=-1){
                System.out.println(planning.get(i));
            }
    }

}
