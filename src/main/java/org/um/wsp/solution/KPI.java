package org.um.wsp.solution;

import org.um.wsp.model.*;
import java.time.Duration;
import java.time.temporal.ChronoUnit;

public class KPI {
    Solution sol;

    public KPI(Solution sol) {
        this.sol = sol;
    }

    public double getKPIRatioJob(){//number of jobs per employee, in a day
        double a = 0.0;
        double b = 0.0;
        for(Edt edt : sol.getListEdt()){
            b+=1.0;
            for(Affectation aff : edt.getPlanning()){
                if(!aff.getJob().isPause()){
                    a+=1;
                }
            }
        }
        if(b>0){
            return a/b;
        }
        else{
            return -1.0;
        }
    }

    public double getKPIRatioUnassigned(){//ratio of unassigned job over total number of jobs
        double a = sol.getUnassigned().size();
        double b = a;
        for(Edt edt : sol.getListEdt()){
            for(Affectation aff : edt.getPlanning()){
                if(!aff.getJob().isPause()){
                    b+=1;
                }
            }//not counting home and breaks
        }
        if(b>0){
            return a/b;
        }
        else{
            return -1.0;
        }
    }

    public int getKPIUnassigned(){ return sol.getUnassigned().size();}

    public Duration getKPIAvgWorkingTime(){//average working time per employee
        Duration a=Duration.ZERO;
        double b = 0.0;
        for(Edt edt : sol.getListEdt()){
            b+=1.0;
            for(Affectation aff : edt.getPlanning()) {
                if(!aff.getJob().isPause()){
                    a=a.plus(aff.getWindow().length());
                }
            }
        }
        if(b>0){
            return a.dividedBy((long)b);
        }
        else{
            return Duration.of(-1, ChronoUnit.HOURS);
        }
    }

    public Duration getKPIAvgDrivingTime(){//average driving time per employee
        Duration a=Duration.ZERO;
        double b = 0.0;
        for(Edt edt : sol.getListEdt()){
            b+=1.0;
            a=a.plus(edt.getDriving());
        }
        if(b>0){
            return a.dividedBy((long)b);
        }
        else{
            return Duration.of(-1, ChronoUnit.HOURS);
        }
    }


    public Duration getKPIAvgIdleTime(){//average idle time per employee
        Duration a = Duration.ZERO;
        double b = 0.0;
        Duration c;
        for(Edt edt : sol.getListEdt()){
            b+=1.0;
            c = Duration.ZERO;
            for(Affectation aff : edt.getPlanning()) {
                c=c.plus(aff.getWindow().length());
            }
            for(TimeInterval ti : edt.getEmp().getShifts()){
                a=a.plus(ti.length());
            }
            a=a.minus(c);
        }
        if(b>0){
            return a.dividedBy((long)b);
        }
        else{
            return Duration.of(-1, ChronoUnit.HOURS);
        }
    }
}

