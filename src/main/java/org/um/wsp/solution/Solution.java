package org.um.wsp.solution;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.um.wsp.model.Job;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class Solution {
    private List<Edt> listEdt;
    private List<Job> unassigned;//not able to assign

    public Solution() {
        this.listEdt = new ArrayList<>();
        this.unassigned = new ArrayList<>();
    }

    public Solution(List<Edt> le,List<Job> lj){
        this.listEdt=le;
        this.unassigned=lj;
    }

    //Getters & Setters

    public List<Edt> getListEdt() {
        return listEdt;
    }

    public void setListEdt(List<Edt> listEdt) {
        this.listEdt = listEdt;
    }

    public List<Job> getUnassigned() {
        return unassigned;
    }

    public void setUnassigned(List<Job> unassigned) {
        this.unassigned = unassigned;
    }

    //TODO think about what can invalid a solution
    public boolean isValid(){
        boolean validEDT=true;//if no edt to check, it's okay
        for(Edt edt: listEdt){
            validEDT=validEDT&&edt.isValid();//valid iff all edt are valid
        }
        return validEDT;
    }

    //lecture from a Json file
    public Solution getFromJSON(String file) throws IOException {
        File f=new File(file);
        Solution solution;
        ObjectMapper mapper=new ObjectMapper();
        mapper.findAndRegisterModules();
        solution=mapper.readValue(f,Solution.class);
        return solution;
    }

    //writing on a JSON file
    public void writeSolution(File f) throws IOException{
        ObjectMapper mapper=new ObjectMapper();
        mapper.findAndRegisterModules();
        mapper.writerWithDefaultPrettyPrinter().writeValue(f,this);
    }

    public void printSolution(){
        for(int i=0;i<(int)listEdt.size();i++){
            listEdt.get(i).printEdt();
            System.out.println();
        }
        System.out.println(unassigned.toString());
    }
}
