package org.um.wsp.model;

import java.util.*;

public class Employee {
    private int id;
    private String name = "";//optional
    private List<TimeInterval> shifts;
    private List<TimeInterval> prefSlots;//optional
    private Position startingLoc;
    private List<Skill> skills;
    //Constructors

    public Employee() {
        this.id = 0;
        this.shifts = new ArrayList<>();
        this.startingLoc = new Position();
        this.skills = new ArrayList<>();
    }

    public Employee(int id, List<TimeInterval> shifts, Position startingLoc, List<Skill> skills) {
        this.id = id;
        this.shifts = shifts;
        this.startingLoc = startingLoc;
        this.skills = skills;
        this.name="";
    }

    //Getters & Setters

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<TimeInterval> getShifts() {
        return shifts;
    }

    public void setShifts(List<TimeInterval> shifts) {
        this.shifts = shifts;
    }

    public List<TimeInterval> getPrefSlots() {
        return prefSlots;
    }

    public void setPrefSlots(List<TimeInterval> prefSlots) {
        this.prefSlots = prefSlots;
    }

    public Position getStartingLoc() {
        return startingLoc;
    }

    public void setStartingLoc(Position startingLoc) {
        this.startingLoc = startingLoc;
    }

    public List<Skill> getSkills() {
        return skills;
    }

    public void setSkills(List<Skill> skills) {
        this.skills = skills;
    }
}
