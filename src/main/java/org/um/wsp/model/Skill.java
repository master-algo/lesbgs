package org.um.wsp.model;

public class Skill {
    private int id;
    private String name;
    private int level;

    public Skill(){
        this.id=0;
        this.name="";
        this.level=0;
    }

    public Skill(int id, String name,int level){
        this.id=id;
        this.name=name;
        this.level=level;
    }

    public Skill(int id,int level){
        this.id=id;
        this.level=level;
    }

    //Getters & Setters

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
