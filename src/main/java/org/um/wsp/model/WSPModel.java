package org.um.wsp.model;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.*;
//This Class will be used to store the problem input
public class WSPModel {
    private List<Job> jobs;
    private List<Employee> employees;
    private List<Appointment> appointments;

    //Constructors

    public WSPModel() {
        this.jobs = new ArrayList<>();
        this.employees = new ArrayList<>();
        this.appointments = new ArrayList<>();
    }

    public WSPModel(List<Job> j, List<Employee> e, List<Appointment> a) {
        this.jobs = j;
        this.employees = e;
        this.appointments = a;
    }

    //Getters & Setters

    public List<Job> getJobs() {
        return jobs;
    }

    public void setJobs(List<Job> jobs) {
        this.jobs = jobs;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public void setAppointments(List<Appointment> appointments) {
        this.appointments = appointments;
    }

    public List<Appointment> getAppointments() {
        return appointments;
    }

    //Add methods

    public void addJob(Job job){
        jobs.add(job);
    }

    public void addEmployee(Employee employee){
        employees.add(employee);
    }

    public void addAppointment(Appointment appointment){
        appointments.add(appointment);
    }

    //lecture from a Json file
    public static WSPModel getFromJSON(String file) throws IOException {
        File f=new File(file);
        WSPModel m;
        ObjectMapper mapper=new ObjectMapper();
        mapper.findAndRegisterModules();
        m=mapper.readValue(f,WSPModel.class);
        return m;
    }

    //writing on a JSON file
    public void writeJSON(File f) throws IOException{
        ObjectMapper mapper=new ObjectMapper();
        mapper.findAndRegisterModules();
        mapper.writerWithDefaultPrettyPrinter().writeValue(f,this);
    }

}
