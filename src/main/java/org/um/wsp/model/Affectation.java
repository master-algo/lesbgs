package org.um.wsp.model;

public class Affectation {
    private Job job;
    private TimeInterval window;

    public Affectation(){
        this.job=new Job();
        this.window=new TimeInterval();
    }

    public Affectation(Job j,TimeInterval t){
        this.job=j;
        this.window=t;
    }

    //Getters & Setters

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public TimeInterval getWindow() {
        return window;
    }

    public void setWindow(TimeInterval window) {
        this.window = window;
    }

    public String toString(){
        return window.getStart().toString()+" "+ job.toString() +" "+window.getEnd().toString();
    }
}
