package org.um.wsp.model;

import java.time.Duration;
import java.time.LocalTime;

public class TimeInterval {
    private LocalTime start;
    private LocalTime end;

    public TimeInterval(LocalTime start,LocalTime end) {
        this.start = start;
        this.end = end;
    }

    public TimeInterval() {
        this.start= LocalTime.of(0,0,0);
        this.end= LocalTime.of(0,0,0);
    }

    //Getters & Setters

    public LocalTime getStart() {
        return start;
    }

    public LocalTime getEnd() {
        return end;
    }

    public void setStart(LocalTime start) {
        this.start = start;
    }

    public void setEnd(LocalTime end) {
        this.end = end;
    }

    // Additional Methods
    public boolean intersect(TimeInterval t){
        return (!end.isBefore(t.start)) && !t.end.isBefore(start);
    }

    public boolean isBefore(TimeInterval t){
        return this.start.isBefore(t.getStart());
    }

    //get useless
    public Duration length(){//number of min in window
        return Duration.between(this.getStart(), this.getEnd());
    }
}
