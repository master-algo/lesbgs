package org.um.wsp.model;

import java.util.*;

public class Appointment {//when an employee is specifically needed for a Job
    private Job job;
    private List<Employee> needed;

    //Constructors

    public Appointment(){
        this.job=new Job();
        this.needed=new ArrayList<>();
    }

    public Appointment(Job j,List<Employee> n){
        this.job=j;
        this.needed=n;
    }

    //Getters & Setters

    public Job getJob() {
        return job;
    }

    public void setJob(Job j) {
        this.job = j;
    }

    public List<Employee> getNeeded() {
        return needed;
    }

    public void setNeeded(List<Employee> n) {
        this.needed = n;
    }
}

