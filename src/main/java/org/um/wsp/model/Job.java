package org.um.wsp.model;

import java.time.Duration;
import java.time.LocalTime;
import java.util.*;

public class Job {
    private int id;
    private String desc = ""; //optional
    private Position location;
    private TimeInterval openings;
    private TimeInterval preferredSlots;//optional
    private Job doAfter; //when a task needs to be done before optional
    private int nbEmpRequired =1;
    private LocalTime deadline;
    private List<Skill> skills; //skills required
    private Duration duration;
    private boolean pause = false;

    public String toString(){
        String s="[Job's Id : "+((Integer)id).toString();
        if(desc!="")
            s=s+", "+desc;
        s=s+"]";
        return s;
    }
    //constructors
    public Job(){
        this.id=0;
        this.location=new Position();
        this.openings=new TimeInterval();
        this.deadline= LocalTime.of(0,0);
        this.skills=new ArrayList<>();
        this.duration= Duration.ZERO;
    }

    public Job(int id, Position location, TimeInterval openings, LocalTime deadline, List<Skill> skills, Duration d) {
        this.id = id;
        this.location = location;
        this.openings = openings;
        this.deadline = deadline;
        this.skills = skills;
        this.duration=d;
    }

    public Job(int id, Position location) {
        this.id = id;
        this.location = location;
        this.skills=new ArrayList<>();
    }

    public Job(Job job){
        this.id = job.id;
        this.location = job.location;
        this.openings = job.openings;
        this.deadline = job.deadline;
        this.skills = job.skills;
        this.duration=job.duration;
    }

    //Getters & Setters

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Position getLocation() {
        return location;
    }

    public void setLocation(Position location) {
        this.location = location;
    }

    public TimeInterval getOpenings() {
        return openings;
    }

    public void setOpenings(TimeInterval openings) {
        this.openings = openings;
    }

    public TimeInterval getPreferredSlots() {
        return preferredSlots;
    }

    public void setPreferredSlots(TimeInterval preferredSlots) {
        this.preferredSlots = preferredSlots;
    }

    public Job getDoAfter() {
        return doAfter;
    }

    public void setDoAfter(Job doAfter) {
        this.doAfter = doAfter;
    }

    public int getNbEmpRequired() {
        return nbEmpRequired;
    }

    public void setNbEmpRequired(int nbEmpRequired) {
        this.nbEmpRequired = nbEmpRequired;
    }

    public LocalTime getDeadline() {
        return deadline;
    }

    public void setDeadline(LocalTime deadline) {
        this.deadline = deadline;
    }

    public List<Skill> getSkills() {
        return skills;
    }

    public void setSkills(List<Skill> skills) {
        this.skills = skills;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public boolean isPause() {
        return pause;
    }

    public void setPause(boolean pause) {
        this.pause = pause;
    }
}
