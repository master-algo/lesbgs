package org.um.wsp.model;

import java.time.Duration;

public class Position {
    private int id;
    private String name; //optional
    private float latitude;
    private float longitude;
    public static final int SPEED = 1;//(meters per minutes)

    //Constructors

    public Position(){
        this.id = 0;
        this.name = "";
        this.latitude = 0.0f;
        this.longitude = 0.0f;
    }

    public Position(int id,float latitude, float longitude) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    //Getters & Setters

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public static double distance(Position p1, Position p2) {
        double r = 6371; // metres
        double lon = p2.getLongitude();
        double lat = p2.getLatitude();
        double x1 = p1.latitude * Math.PI / 180;
        double x2 = lat * Math.PI / 180;
        double y1 = (lat - p1.latitude) * Math.PI / 180;
        double y2 = (lon - p1.longitude) * Math.PI / 180;

        double a = Math.sin(y1 / 2) * Math.sin(y1 / 2) + Math.cos(x1) * Math.cos(x2) * Math.sin(y2 / 2) * Math.sin(y2 / 2);
        double b = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return r * b;// in metres
        }

    public static Duration travelTime(Position p1, Position p2){//number of minutes
        return Duration.ofMinutes((long) (1+distance(p1,p2)/SPEED));
    }
}
