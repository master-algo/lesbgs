import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.um.wsp.gen.Generator;
import org.um.wsp.model.*;
import org.um.wsp.solution.Edt;
import org.um.wsp.solution.Solution;
import org.um.wsp.solver.*;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

class SerializationTest {
    @Test
    //ok
    void testMapper() {
        //ObjectMapper mapper=new ObjectMapper();
        File f=new File("test.json");
        WSPModel model=new WSPModel();
        //initialisation job
        Position pos=new Position(1,44.2752f, 4.1254f);
        pos.setName("New York");
        LocalTime start = LocalTime.of(1,9,0);
        LocalTime end = LocalTime.of(1,12,15);
        TimeInterval openings=new TimeInterval(start,end);
        List<Skill> skillsJ=new ArrayList<>();
        Skill skill=new Skill(666,"drinking",666);//bg
        skillsJ.add(skill);
        Duration t= Duration.of(1, ChronoUnit.HOURS);
        Job job=new Job(13,pos,openings,end,skillsJ,t);
        model.addJob(job);
        //initialisation employee
        List<TimeInterval> shifts=new ArrayList<>();
        LocalTime startShift = LocalTime.of(1,9,0);
        LocalTime endShift = LocalTime.of(1,12,15);
        TimeInterval s=new TimeInterval(startShift,endShift);
        shifts.add(s);
        Position position=new Position(1,44.3714f,4.2328f);
        position.setName("Los Angeles");
        List<Skill> skillsE=new ArrayList<>();
        skillsE.add(skill);
        Employee employee=new Employee(700,shifts,position,skillsE);
        model.addEmployee(employee);
        model.addEmployee(employee);//test 2 employee
        //initialisation appointment
        List<Employee> needed=new ArrayList<>();
        needed.add(employee);
        Appointment appointment=new Appointment(job,needed);
        //fin initialisations
        model.addAppointment(appointment);//same
        //Assertions.assertDoesNotThrow(() -> mapper.writerWithDefaultPrettyPrinter().writeValue(f,model));
        Assertions.assertDoesNotThrow(() -> model.writeJSON(f));
        Assertions.assertTrue(f.exists());
    }

    @Test
    //ok
    void readModel() throws IOException {
        WSPModel model=new WSPModel();
        model.getFromJSON("instance-Xarry.json");
        Assertions.assertNotNull(model);
    }

    @Test
    //ok
    void checkSort(){
        Affectation a= Generator.randomAffectation();
        Affectation b= Generator.randomAffectation();
        boolean x=a.getWindow().isBefore(b.getWindow());
        List<Affectation> l=new ArrayList<>();
        l.add(a);
        l.add(b);
        Employee e= Generator.randomEmp();
        Edt ed=new Edt(e,l);
        boolean y=ed.sortAffectation();
        Assertions.assertTrue(y || x);
    }

    @Test
    //ok
    void timeBefore(){
        LocalTime t1= LocalTime.of(2,10,3);
        LocalTime t2= LocalTime.of(2,10,4);
        boolean x=t1.isBefore(t2);
        Assertions.assertTrue(x);
    }

    @Test
    //ok
    void distance(){
        Position p1=new Position(1,43.3643f,3.5238f);
        Position p2=new Position(2,44.2752f,4.1254f);
        double x=Position.distance(p1,p2);
        Assertions.assertTrue(x>=110 && x<=115);
    }

    @Test
    void travelTime(){
        Position paris = new Position(8,48.80f,2.38f);
        Position montpellier = new Position(7,43.61f,3.88f);
        double dist=Position.distance(paris,montpellier);
        System.out.println("Distance : "+dist);
        Assertions.assertTrue(550<dist && dist<600);
        Duration travelTime=Position.travelTime(paris,montpellier);
        System.out.println(travelTime);
        //Assertions.assertTrue();
    }

    @Test
    void canDo(){
        int id=1;
        List<TimeInterval> sh=new ArrayList<>();
        TimeInterval t=new TimeInterval(LocalTime.of(8,0),LocalTime.of(20,0));
        sh.add(t);//8H to 20h
        Position st=new Position();// 0 0 0
        List<Skill> sk=new ArrayList<>();
        Skill s=new Skill(3,"MANAGEMENT",3);
        sk.add(s);
        Employee e=new Employee(id,sh,st,sk);
        Position p=new Position(1,50.0f,50.0f);
        List<Skill> sk1=new ArrayList<>();
        s=new Skill(3,"MANAGEMENT",2);
        sk1.add(s);
        Job j=new Job(2,p,t,LocalTime.of(13,0),sk1,Duration.ofHours(1));
        boolean b= Compatibility.canDo(e,j);
        Assertions.assertTrue(b);
    }

    /*
    ____________________________________________
       i n s t a n c e    G e n e r a t i o n
    ____________________________________________
    */

    @Test
    void generateInstance() throws IOException {
        List<Tuple> lt=param();
        int nbJob,nbEmp,nbApp;
        WSPModel m;
        File f;
        for (Tuple tuple : lt) {
            nbJob = tuple.getJob();
            nbEmp = tuple.getEmp();
            nbApp = tuple.getApp();
            m = Generator.modelRandom(nbJob, nbEmp, nbApp);
            f = new File("instanceRand-" + nbJob + "-" + nbEmp + "-" + nbApp + ".json");
            m.writeJSON(f);
            Assertions.assertTrue(f.exists());
        }
    }

    /*
    ____________________________________________
       i n s t a n c e    r e s o l u t i o n
    ____________________________________________
    */

    List<Tuple> EmpParam(int nbJob,int nbEmp){
        List<Tuple> p=new ArrayList<>();
        p.add(new Tuple(nbJob,nbEmp,0));
        p.add(new Tuple(nbJob,nbEmp,2));
        p.add(new Tuple(nbJob,nbEmp,5));
        p.add(new Tuple(nbJob,nbEmp,10));
        return p;
    }

    List<Tuple> JobParam(int nbJob){
        List<Tuple> p=new ArrayList<>();
        p.addAll(EmpParam(nbJob,2));
        p.addAll(EmpParam(nbJob,5));
        p.addAll(EmpParam(nbJob,10));
        p.addAll(EmpParam(nbJob,20));
        return p;
    }

    List<Tuple> param(){
        List<Tuple> p=new ArrayList<>();
        p.addAll(JobParam(10));
        p.addAll(JobParam(20));
        p.addAll(JobParam(50));
        p.addAll(JobParam(100));
        return p;
    }

    @Test//ok
    void paramTest(){
        List<Tuple> p=param();
        System.out.println(p.toString());
        Assertions.assertEquals(4*4*4,p.size());
    }

    @Test
    void heuristic() throws IOException {
        Instant start,end;
        int nbJob,nbEmp,nbApp;
        long millis;
        double seconds;
        boolean print=false;//print solutions ?
        String h="J";//J for Jojo's heuristic, T for Trist's heuristic
        String st="Duration";//Duration, Skill, Deadline
        Solution s;
        List<Tuple> lt=param();
        for (Tuple tuple : lt) {
            start = Instant.now();
            nbJob = tuple.getJob();
            nbEmp = tuple.getEmp();
            nbApp = tuple.getApp();
            s = Heuristic.heuristic(nbJob, nbEmp, nbApp, st, print, h);
            end = Instant.now();
            millis = Duration.between(start, end).toMillis();
            seconds = millis / 1000.0;
            System.out.println(" & " + seconds + "s ");
            Assertions.assertTrue(s.isValid());
        }
    }

    /*
    ____________________________________________
                    P r i n t
    ____________________________________________
    */

    @Test//ok
    void testPrintSol() throws IOException {
        String nameFile="instance-Xarry.json";
        Solution s;
        WSPModel m=new WSPModel().getFromJSON(nameFile);
        int mj=m.getJobs().size();
        HeuristicSolver h= new HeuristicSolver(m);
        s=h.solve();
        Assertions.assertTrue(s.isValid());
        s.printSolution();
        System.out.println(s.getUnassigned().size());
        System.out.println("nb of jobs assigned : " + ((Integer)(mj-s.getUnassigned().size())));
    }

    /*
    ____________________________________________
        i n s t a n c e   r e a l i s t i c
    ____________________________________________
    */

    public WSPModel genInstanceReal() throws IOException {
        //init
        List<Job> jobs = new ArrayList<>();
        List<Employee> employees = new ArrayList<>();
        List<Appointment> appointments = new ArrayList<>();

        //Positions
        Position lattes = new Position(0,43.56f,3.89f);
        lattes.setName("Lattes");
        Position ales = new Position(1,44.12f,4.08f);
        Position baillargues = new Position(2,43.66f,4.01f);
        Position ganges = new Position(3,43.93f,3.71f);
        Position lagrandemotte = new Position(4,43.56f,4.07f);
        Position lodeve = new Position(5,43.73f,3.31f);
        Position lunel = new Position(6,43.67f,4.13f);
        Position montpellier = new Position(7,43.61f,3.88f);
        Position nimes = new Position(8,43.83f,4.35f);
        Position sete = new Position(9,43.40f,3.69f);
        Position sommieres = new Position(10,43.78f,4.09f);
        Position vergeze = new Position(11,43.74f,4.21f);

        //Skills
        Skill electrician1 = new Skill(0,1);
        Skill electrician2 = new Skill(0,2);
        Skill electrician3 = new Skill(0,3);
        Skill engineer1 = new Skill(1,1);
        Skill engineer2 = new Skill(1,2);
        Skill engineer3 = new Skill(1,3);
        Skill management1 = new Skill(2,1);
        Skill plumbing1 = new Skill(3,1);
        Skill plumbing2 = new Skill(3,2);
        Skill masonry1 = new Skill(4,1);
        Skill masonry2 = new Skill(4,2);
        Skill masonry3 = new Skill(4,3);
        Skill masonry4 = new Skill(4,4);
        Skill carpentry1 = new Skill(5,1);
        Skill carpentry2 = new Skill(5,2);
        Skill painting1 = new Skill(6,1);
        Skill glazing1 = new Skill(7,1);
        Skill ironwork1 = new Skill(8,1);
        Skill ironwork2 = new Skill(8,2);
        Skill ironwork3 = new Skill(8,3);
        Skill ironwork4 = new Skill(8,4);
        Skill tiler1 = new Skill(9,1);

        //Employees
        List<TimeInterval> standard = new ArrayList<>();//standard day
        standard.add(new TimeInterval(LocalTime.of(9,0),LocalTime.of(12,0)));//9h-12h
        standard.add(new TimeInterval(LocalTime.of(13,0),LocalTime.of(17,0)));//13h-17h

        List<Skill> l_l = new ArrayList<>();
        l_l.add(masonry4);
        l_l.add(carpentry2);
        l_l.add(plumbing1);
        Employee larry = new Employee(0,standard,lattes,l_l);
        larry.setName("Larry");
        employees.add(larry);

        List<Skill> l_b = new ArrayList<>();
        l_b.add(painting1);
        l_b.add(glazing1);
        l_b.add(electrician3);
        Employee barry = new Employee(1,standard,lattes,l_b);
        barry.setName("Barry");
        employees.add(barry);

        List<Skill> l_h = new ArrayList<>();
        l_h.add(plumbing2);
        l_h.add(ironwork1);
        l_h.add(carpentry1);
        Employee harry = new Employee(2,standard,lattes,l_h);
        harry.setName("Harry");
        employees.add(harry);

        List<Skill> l_m = new ArrayList<>();
        l_m.add(ironwork3);
        l_m.add(engineer3);
        l_m.add(electrician1);
        l_m.add(masonry2);
        Employee mary = new Employee(3,standard,lattes,l_m);
        mary.setName("Mary");
        employees.add(mary);

        List<Skill> l_c = new ArrayList<>();
        l_c.add(engineer2);
        l_c.add(management1);
        Employee cary = new Employee(4,standard,lattes,l_c);
        cary.setName("Cary");
        employees.add(cary);

        List<Skill> l_g = new ArrayList<>();
        l_g.add(tiler1);
        l_g.add(painting1);
        Employee gary = new Employee(5,standard,lattes,l_g);
        gary.setName("Gary");
        employees.add(gary);

        //Jobs
        //ales, 1h, 10h-15h
        List<Skill> l_al1 = new ArrayList<>();
        l_al1.add(electrician2);
        Job ales1 = new Job(0,ales,new TimeInterval(LocalTime.of(10,0),LocalTime.of(15,0)),LocalTime.of(15,0),l_al1,Duration.ofHours(1));
        ales1.setDesc("Ales: Electrician Lvl2");
        jobs.add(ales1);

        //ales, 1h, 13h-17h
        List<Skill> l_al2 = new ArrayList<>();
        l_al2.add(painting1);
        Job ales2 = new Job(1,ales,new TimeInterval(LocalTime.of(13,0),LocalTime.of(17,0)),LocalTime.of(17,0),l_al2,Duration.ofHours(1));
        ales2.setDesc("Ales: Painting");
        jobs.add(ales2);

        //ales, 30min, 13h-17h
        List<Skill> l_al3 = new ArrayList<>();
        l_al3.add(ironwork1);
        Job ales3 = new Job(22,ales,new TimeInterval(LocalTime.of(13,0),LocalTime.of(17,0)),LocalTime.of(17,0),l_al3,Duration.ofMinutes(30));
        ales3.setDesc("Ales: Ironwork");
        jobs.add(ales3);

        //baillargues, 30min, 9h-17h
        List<Skill> l_ba1 = new ArrayList<>();
        l_ba1.add(plumbing1);
        Job baillargues1 = new Job(2,baillargues,new TimeInterval(LocalTime.of(9,0),LocalTime.of(17,0)),LocalTime.of(17,0),l_ba1,Duration.ofMinutes(30));
        baillargues1.setDesc("Baillargues: Plumbing");
        jobs.add(baillargues1);

        //baillargues, 1h30, 13h-17h
        List<Skill> l_ba2 = new ArrayList<>();
        l_ba2.add(tiler1);
        Job baillargues2 = new Job(3,baillargues,new TimeInterval(LocalTime.of(13,0),LocalTime.of(17,0)),LocalTime.of(17,0),l_ba2,Duration.ofMinutes(90));
        baillargues2.setDesc("Baillargues: Tiler");
        jobs.add(baillargues2);

        //baillargues, 30min, 13h-17h
        List<Skill> l_ba3 = new ArrayList<>();
        l_ba3.add(electrician1);
        Job baillargues3 = new Job(23,baillargues,new TimeInterval(LocalTime.of(13,0),LocalTime.of(17,0)),LocalTime.of(17,0),l_ba3,Duration.ofMinutes(30));
        baillargues3.setDesc("Baillargues: Electrician");
        jobs.add(baillargues3);

        //ganges, 45min, 9h-12h
        List<Skill> l_ga1 = new ArrayList<>();
        l_ga1.add(management1);
        Job ganges1 = new Job(4,ganges,new TimeInterval(LocalTime.of(9,0),LocalTime.of(12,0)),LocalTime.of(12,0),l_ga1,Duration.ofMinutes(45));
        ganges1.setDesc("Ganges: Management");
        jobs.add(ganges1);

        //ganges, 30min, 9h-17h
        List<Skill> l_ga2 = new ArrayList<>();
        l_ga2.add(plumbing2);
        Job ganges2 = new Job(5,ganges,new TimeInterval(LocalTime.of(9,0),LocalTime.of(17,0)),LocalTime.of(17,0),l_ga2,Duration.ofMinutes(30));
        ganges2.setDesc("Ganges: Plumbing Lvl2");
        jobs.add(ganges2);

        //ganges, 30min, 13h-17h
        List<Skill> l_ga3 = new ArrayList<>();
        l_ga3.add(plumbing1);
        Job ganges3 = new Job(24,ganges,new TimeInterval(LocalTime.of(13,0),LocalTime.of(17,0)),LocalTime.of(17,0),l_ga2,Duration.ofMinutes(30));
        ganges3.setDesc("Ganges: Plumbing");
        jobs.add(ganges3);

        //lagrandemotte, 1h, 14h-16h
        List<Skill> l_la1 = new ArrayList<>();
        l_la1.add(management1);
        Job lagrandemotte1 = new Job(6,lagrandemotte,new TimeInterval(LocalTime.of(14,0),LocalTime.of(16,0)),LocalTime.of(16,0),l_la1,Duration.ofHours(1));
        lagrandemotte1.setDesc("La Grande Motte: Management");
        jobs.add(lagrandemotte1);

        //lagrandemotte, 1h, 9h-15h
        List<Skill> l_la2 = new ArrayList<>();
        l_la2.add(glazing1);
        Job lagrandemotte2 = new Job(7,lagrandemotte,new TimeInterval(LocalTime.of(9,0),LocalTime.of(15,0)),LocalTime.of(15,0),l_la2,Duration.ofHours(1));
        lagrandemotte2.setDesc("La Grande Motte: Glazing");
        jobs.add(lagrandemotte2);

        //lagrandemotte, 30min, 13h-17h
        List<Skill> l_la3 = new ArrayList<>();
        l_la3.add(engineer1);
        Job lagrandemotte3 = new Job(25,lagrandemotte,new TimeInterval(LocalTime.of(13,0),LocalTime.of(17,0)),LocalTime.of(17,0),l_la3,Duration.ofMinutes(30));
        lagrandemotte3.setDesc("La Grande Motte: Engineer");
        jobs.add(lagrandemotte3);

        //lodeve, 2h, 9h-17h
        List<Skill> l_lo1 = new ArrayList<>();
        l_lo1.add(masonry1);
        Job lodeve1 = new Job(8,lodeve,new TimeInterval(LocalTime.of(9,0),LocalTime.of(17,0)),LocalTime.of(17,0),l_lo1,Duration.ofHours(2));
        lodeve1.setDesc("Lodeve: Masonry");
        jobs.add(lodeve1);

        //lodeve, 1h, 9h-17h
        List<Skill> l_lo2 = new ArrayList<>();
        l_lo2.add(painting1);
        Job lodeve2 = new Job(20,lodeve,new TimeInterval(LocalTime.of(9,0),LocalTime.of(17,0)),LocalTime.of(17,0),l_lo2,Duration.ofHours(1));
        lodeve2.setDesc("Lodeve: Painting");
        jobs.add(lodeve2);

        //lunel, 30min, 13h-16h
        List<Skill> l_lu1 = new ArrayList<>();
        l_lu1.add(plumbing1);
        Job lunel1 = new Job(9,lunel,new TimeInterval(LocalTime.of(13,0),LocalTime.of(16,0)),LocalTime.of(16,0),l_lu1,Duration.ofMinutes(30));
        lunel1.setDesc("Lunel: Plumbing");
        jobs.add(lunel1);

        //lunel, 2h, 9h-17h
        List<Skill> l_lu2 = new ArrayList<>();
        l_lu2.add(carpentry2);
        Job lunel2 = new Job(10,lunel,new TimeInterval(LocalTime.of(9,0),LocalTime.of(17,0)),LocalTime.of(17,0),l_lu2,Duration.ofHours(2));
        lunel2.setDesc("Lunel: Carpentry Lvl2");
        jobs.add(lunel2);

        //lunel, 30min, 11h-17h
        List<Skill> l_lu3 = new ArrayList<>();
        l_lu3.add(masonry3);
        Job lunel3 = new Job(26,lunel,new TimeInterval(LocalTime.of(11,0),LocalTime.of(17,0)),LocalTime.of(17,0),l_lu3,Duration.ofMinutes(30));
        lunel3.setDesc("Lunel: Masonry Lvl3");
        jobs.add(lunel3);

        //montpellier, 1h30, 13h-17h
        List<Skill> l_mo1 = new ArrayList<>();
        l_mo1.add(painting1);
        l_mo1.add(glazing1);
        Job montpellier1 = new Job(11,montpellier,new TimeInterval(LocalTime.of(13,0),LocalTime.of(17,0)),LocalTime.of(17,0),l_mo1,Duration.ofMinutes(90));
        montpellier1.setDesc("Montpellier: Painting & Glazing");
        jobs.add(montpellier1);

        //montpellier, 45min, 9h-12h
        List<Skill> l_mo2 = new ArrayList<>();
        l_mo2.add(plumbing2);
        Job montpellier2 = new Job(12,montpellier,new TimeInterval(LocalTime.of(9,0),LocalTime.of(12,0)),LocalTime.of(12,0),l_mo2,Duration.ofMinutes(45));
        montpellier2.setDesc("Montpellier: Plumbing Lvl2");
        jobs.add(montpellier2);

        //montpellier, 30min, 13h-17h
        List<Skill> l_mo3 = new ArrayList<>();
        l_mo3.add(masonry1);
        Job montpellier3 = new Job(27,montpellier,new TimeInterval(LocalTime.of(13,0),LocalTime.of(17,0)),LocalTime.of(17,0),l_mo3,Duration.ofMinutes(30));
        montpellier3.setDesc("Montpellier: Masonry");
        jobs.add(montpellier3);

        //montpellier, 30min, 14h30-17h
        List<Skill> l_mo4 = new ArrayList<>();
        l_mo4.add(carpentry1);
        Job montpellier4 = new Job(28,montpellier,new TimeInterval(LocalTime.of(14,30),LocalTime.of(17,0)),LocalTime.of(17,0),l_mo4,Duration.ofMinutes(30));
        montpellier4.setDesc("Montpellier: Carpentry");
        jobs.add(montpellier4);

        //nimes, 30min, 9h-17h
        List<Skill> l_ni1 = new ArrayList<>();
        l_ni1.add(masonry4);
        l_ni1.add(carpentry1);
        Job nimes1 = new Job(13,nimes,new TimeInterval(LocalTime.of(9,0),LocalTime.of(17,0)),LocalTime.of(17,0),l_ni1,Duration.ofMinutes(30));
        nimes1.setDesc("Nimes: Masonry Lvl4 & Carpentry");
        jobs.add(nimes1);

        //nimes, 1h, 9h-12h
        List<Skill> l_ni2 = new ArrayList<>();
        l_ni2.add(glazing1);
        l_ni2.add(electrician1);
        Job nimes2 = new Job(14,nimes,new TimeInterval(LocalTime.of(9,0),LocalTime.of(12,0)),LocalTime.of(12,0),l_ni2,Duration.ofHours(1));
        nimes2.setDesc("Nimes: Glazing & Electrician");
        jobs.add(nimes2);

        //sete, 1h, 9h-16h
        List<Skill> l_se1 = new ArrayList<>();
        l_se1.add(engineer2);
        l_se1.add(ironwork2);
        Job sete1 = new Job(15,sete,new TimeInterval(LocalTime.of(9,0),LocalTime.of(16,0)),LocalTime.of(16,0),l_se1,Duration.ofHours(1));
        sete1.setDesc("Sete: Engineer Lvl2 & Ironwork Lvl2");
        jobs.add(sete1);

        //sete, 1h30, 10h-17h
        List<Skill> l_se2 = new ArrayList<>();
        l_se2.add(carpentry1);
        Job sete2 = new Job(16,sete,new TimeInterval(LocalTime.of(10,0),LocalTime.of(17,0)),LocalTime.of(17,0),l_se2,Duration.ofMinutes(90));
        sete2.setDesc("Sete: Carpentry");
        jobs.add(sete2);

        //sete, 30min, 13h-17h
        List<Skill> l_se3 = new ArrayList<>();
        l_se3.add(ironwork1);
        Job sete3 = new Job(29,sete,new TimeInterval(LocalTime.of(13,0),LocalTime.of(17,0)),LocalTime.of(17,0),l_se3,Duration.ofMinutes(30));
        sete3.setDesc("Sete: Ironwork");
        jobs.add(sete3);

        //sommieres, 1h, 9h-12h
        List<Skill> l_so1 = new ArrayList<>();
        l_so1.add(tiler1);
        Job sommieres1 = new Job(17,sommieres,new TimeInterval(LocalTime.of(9,0),LocalTime.of(12,0)),LocalTime.of(12,0),l_so1,Duration.ofHours(1));
        sommieres1.setDesc("Sommieres: Tiler");
        jobs.add(sommieres1);

        //sommieres, 30min, 9h-17h
        List<Skill> l_so2 = new ArrayList<>();
        l_so2.add(management1);
        Job sommieres2 = new Job(21,sommieres,new TimeInterval(LocalTime.of(9,0),LocalTime.of(17,0)),LocalTime.of(17,0),l_so2,Duration.ofMinutes(30));
        sommieres2.setDesc("Sommieres: Management");
        jobs.add(sommieres2);

        //vergeze, 1h, 9h-17h
        List<Skill> l_ve1 = new ArrayList<>();
        l_ve1.add(ironwork4);
        Job vergeze1 = new Job(18,vergeze,new TimeInterval(LocalTime.of(9,0),LocalTime.of(17,0)),LocalTime.of(17,0),l_ve1,Duration.ofHours(1));
        vergeze1.setDesc("Vergeze: Ironwork Lvl4");
        jobs.add(vergeze1);

        //vergeze, 45min, 11h-15h
        List<Skill> l_ve2 = new ArrayList<>();
        l_ve2.add(electrician3);
        l_ve2.add(painting1);
        Job vergeze2 = new Job(19,vergeze,new TimeInterval(LocalTime.of(11,0),LocalTime.of(15,0)),LocalTime.of(15,0),l_ve2,Duration.ofMinutes(45));
        vergeze2.setDesc("Vergeze: Electrician Lvl3 & Painting");
        jobs.add(vergeze2);

        //Model
        WSPModel model=new WSPModel(jobs, employees, appointments);
        File f;
        f=new File("instance-Xarry.json");
        model.writeJSON(f);
        return model;

    }

    @Test//ok
    void testReal() throws IOException{
        WSPModel m=genInstanceReal();
        Instant start,end;
        long millis;
        double seconds;
        Solution s;
        String x="J";
        File f;
        String st="";
        start = Instant.now();
        if(x.equals("J")) {
            st+="Skill";//Duration, Skill, Deadline
            s = HeuristicJojo.solve(m,st);
        }
        else {
            HeuristicSolver h = new HeuristicSolver(m);
            s = h.solve();
        }
        end = Instant.now();
        millis = Duration.between(start, end).toMillis();
        seconds = millis / 1000.0;
        System.out.println(" & " + seconds + "s ");
        f=new File(st+"SoluceT-Xarry.json");
        s.writeSolution(f);
        s.printSolution();
        Assertions.assertTrue(f.exists());
        Heuristic.printKPIs(s,"instance-Xarry.json",false);
    }

}
