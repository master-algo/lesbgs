package org.um.wsp.solver;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.um.wsp.model.WSPModel;
import org.um.wsp.solution.KPI;
import org.um.wsp.solution.Solution;

import java.io.File;
import java.io.IOException;

class MIPSolverTest {

    @Test
    void mip() throws IOException {
        MIPSolver m = new MIPSolver();
        String nameFile="instanceRand-100-20-5.json";
       WSPModel model = WSPModel.getFromJSON(nameFile);
        Solution s= m.solveMIP(model, 300000);
        boolean x=s.isValid();
        File f=new File("testMIP-100-20-5.json");
        s.printSolution();
        Heuristic.printKPIs(s,"instance-100-20-5", false);
        s.writeSolution(f);
        Assertions.assertTrue(x);

    }

}